namespace Spacelaunch {
    [GtkTemplate (ui = "/org/emilien/SpaceLaunch/ui/pageVueCompacte.ui")]
    public class VueCompacte : Gtk.Box {

        [GtkChild]
        public unowned Gtk.ScrolledWindow scrollVueCompacte;
        [GtkChild]
        public unowned Adw.Clamp clamp;
        [GtkChild]
        public unowned Gtk.Box listeCompacte;
        [GtkChild]
        public unowned Gtk.Revealer revealBoutonPlusCompact;
        [GtkChild]
        public unowned Gtk.Button boutonPlusCompact;
        [GtkChild]
        public unowned Gtk.Spinner spinPlus;

        public Window win {get; set;}

        public VueCompacte (Window win) {
            Object (
                win: win
            );

            scrollVueCompacte.edge_reached.connect(affichageBoutonPlusCompact);
            boutonPlusCompact.clicked.connect(() => {
                if(!win.blocageRafraichissement){
                    spinPlus.show();
                    boutonPlusCompact.get_style_context().remove_class("suggested-action");
                    win.appelRafraichissementCompact();
                    boutonPlusCompact.get_style_context().add_class("suggested-action");
                    spinPlus.hide();
                }
            });
        }

        construct {
            clamp.set_maximum_size(600);
            spinPlus.start();
            spinPlus.hide();
        }


		public void disposition(string[] liste) {

            Gtk.CssProvider[] css = chargementCss();
            Pango.AttrList attrs = new Pango.AttrList ();
            attrs.insert (Pango.attr_weight_new (LIGHT));
            Pango.AttrList attrs2 = new Pango.AttrList ();
            attrs2.insert (Pango.attr_weight_new (LIGHT));
            attrs2.insert (Pango.attr_scale_new (0.8));
            Pango.AttrList attrs3 = new Pango.AttrList ();
            attrs3.insert (Pango.attr_weight_new (LIGHT));
            attrs3.insert (Pango.attr_scale_new (0.8));

            Gtk.ListBox[] listeBox;// = new Gtk.ListeBox[];
            listeBox = {};

            //RECHERCHE NOMBRE DE LIGNES DÉJÀ REMPLIES
            int nbLignesCompact = 0;

            //COMPTAGE DU NOMBRE DE LIGNES

            Gtk.Widget widget = listeCompacte.get_first_child();
            while(widget!=null){
                widget=widget.get_next_sibling();
                nbLignesCompact++;
            }

            for(int i=nbLignesCompact; i<win.nbLancement;i++){

                //DÉFINITION DES BLOCS

                Adw.PreferencesGroup groupe = new Adw.PreferencesGroup();
                Adw.PreferencesRow row = new Adw.PreferencesRow();
                Gtk.ListBox ligne0 = new Gtk.ListBox();
                listeBox += ligne0;
                Gtk.ListBoxRow ligne = new Gtk.ListBoxRow();

                //IMAGE LANCEUR

	            string url = lectureCle("image",liste[i]);
                int taille = 84;
                Gtk.Picture image = decoupeImage(url, taille, 0, true);

                //LABELS

                string mission = lectureCle("name", liste[i]);
                int delimiteur = mission.index_of_char('|');
                mission = mission.substring(delimiteur+2);
                Gtk.Label labelMission = new Gtk.Label(mission);
                Gtk.Label rocket = new Gtk.Label(lectureCle("name",lectureBloc("rocket",liste[i])));
                Gtk.Label location = new Gtk.Label(lectureCle("name",lectureBloc("location",liste[i])));
                Gtk.Label date = new Gtk.Label(stringToDateTime(lectureCle("net",liste[i])).format("%d %B %Y"));
                string status = lectureCle("abbrev",lectureBloc("status",liste[i]));

                //STATUT

                Gtk.Picture picStatus = new Gtk.Picture();
                Gdk.Pixbuf pixStatus = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/status_na.svg");
                if(status == "Success") pixStatus = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/status_success.svg");
                if(status == "In Flight") pixStatus = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/status_in_flight.svg");
                if(status == "Go") pixStatus = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/status_go.svg");
                if(status == "TBC") pixStatus = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/status_tbc.svg");
                if(status == "TBD") pixStatus = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/status_tbd.svg");
                if(status == "Hold") pixStatus = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/status_hold.svg");
                if(status == "Failure") pixStatus = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/status_failure.svg");

                picStatus.set_pixbuf(pixStatus);
                picStatus.set_size_request(26,84);

                picStatus.get_style_context().add_provider(css[6], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                picStatus.set_overflow(HIDDEN);

                //DÉFINITIONS DES BOXS

                Gtk.Box boite = new Gtk.Box(Gtk.Orientation.HORIZONTAL,2);
                Gtk.Box boite0 = new Gtk.Box(Gtk.Orientation.VERTICAL,3);

                //MISE EN FORME DES BLOCS

                if(i==0) groupe.set_margin_top(5);
                groupe.set_margin_bottom(5);
                groupe.set_margin_start(5);
                groupe.set_margin_end(5);

                ligne0.get_style_context().add_provider(css[5], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                ligne.get_style_context().add_provider(css[5], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

                //MISE EN FORME DES BOITES

                boite.set_spacing(-2);
                boite0.set_margin_start(10);
                boite0.set_hexpand(true);

                //MISE EN FORME DES LABELS

                rocket.set_attributes(attrs2);
                location.set_attributes(attrs2);
                date.set_attributes(attrs2);

                labelMission.set_halign(Gtk.Align.START);
                rocket.set_halign(Gtk.Align.START);
                location.set_halign(Gtk.Align.START);
                date.set_halign(Gtk.Align.START);
                labelMission.set_ellipsize(Pango.EllipsizeMode.END);
                rocket.set_ellipsize(Pango.EllipsizeMode.END);
                location.set_ellipsize(Pango.EllipsizeMode.END);
                date.set_ellipsize(Pango.EllipsizeMode.END);

                //DISPOSITION DES BOXS

                boite.append(picStatus);
                boite.append(image);
                boite.append(boite0);

                boite0.append(labelMission);
                boite0.append(rocket);
                boite0.append(location);
                boite0.append(date);

                //DISPOSITION DES BLOCS

                groupe.add(row);
                row.set_child(ligne0);
                ligne0.append(ligne);
                ligne.set_child(boite);

                ligne0.get_row_at_index(0).set_selectable(false);
                ligne0.set_name(lectureCle("id",liste[i]));

                listeCompacte.append(groupe);
            }
            for(int i=0; i<listeBox.length; i++){
                listeBox[i].row_activated.connect(win.appelLancement);
            }
        }

        private Gtk.Picture decoupeImage(string url, int taille, int retrait, bool arrondi) {

            Gtk.Picture image = new Gtk.Picture();

	        string nom = dossierCache + url.substring(url.last_index_of_char('/'));

	        Gdk.Pixbuf pix = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/erreurFichier.svg",taille,taille,true);
            pix =  new Gdk.Pixbuf.subpixbuf (pix,retrait,0,taille-(2*retrait),taille);

            if(url != "null"){
	    	    downloadImage(url,nom);
	    	    try{Gdk.Pixbuf pix1 = new Gdk.Pixbuf.from_file(nom);

	    	        int largeur = pix1.get_width();
	    	        int hauteur = pix1.get_height();

    	    	    if(largeur >= hauteur){
	        		    largeur = -1;
	        		    hauteur = taille;
	        	    }
	        	    else{
	        		    largeur = taille;
	        		    hauteur = -1;
	        	    }
                    try{Gdk.Pixbuf pix0 = new Gdk.Pixbuf.from_file_at_scale(nom,largeur,hauteur,true);

                    largeur = pix0.get_width();
                    hauteur = pix0.get_height();

                    int debutX = retrait;
                    int debutY = 0;

                    if(largeur > hauteur){
                        debutX = (int) (largeur -taille + (2*retrait))/2;
                    }
                    else if(largeur < hauteur){
                        debutY = (int) (hauteur -taille)/2;
                    }
                    pix =  new Gdk.Pixbuf.subpixbuf (pix0,debutX,debutY,taille-(2*retrait),taille);} catch(ThreadError e){}
                    } catch(ThreadError e){}
            }

            image.set_pixbuf(pix);
	    	image.set_size_request(84, 84);
            if(arrondi) {
                var css = new Gtk.CssProvider();
                uint8[] coinsArrondisD = "* { border-top-right-radius: 10px; } * { border-bottom-right-radius: 10px; }".data;
                css.load_from_data(coinsArrondisD);
                image.get_style_context().add_provider(css, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                image.set_overflow(HIDDEN);
            }

	        return image;
        }

        private void affichageBoutonPlusCompact(Gtk.PositionType position) {

            if(position == Gtk.PositionType.BOTTOM && win.offset<100){
		        revealBoutonPlusCompact.set_reveal_child(true);
		    }
		    else if(position == Gtk.PositionType.TOP){
		        revealBoutonPlusCompact.set_reveal_child(false);
		    }
		}

		public void effacementPage() {

            Gtk.Widget child = listeCompacte.get_first_child();
		    while (child!=null){
		        listeCompacte.remove(child);
		        child = listeCompacte.get_first_child();
		    }
		    win.offset = 0; win.nbLancement = 10;
        }

        public bool pageVide() {

            bool vide = false;
            Gtk.Widget child = listeCompacte.get_first_child();
            if(child==null) vide = true;

            return vide;
        }
    }
}
