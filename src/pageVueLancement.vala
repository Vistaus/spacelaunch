namespace Spacelaunch {
    [GtkTemplate (ui = "/org/emilien/SpaceLaunch/ui/pageVueLancement.ui")]
    public class VueLancement : Gtk.Box {

        [GtkChild]
        public unowned Adw.Clamp clamp;
        [GtkChild]
        public unowned Adw.PreferencesGroup groupMission;
        [GtkChild]
        public unowned Adw.PreferencesGroup groupDetailsMission;
        [GtkChild]
        public unowned Adw.PreferencesRow rowMission;
        [GtkChild]
        public unowned Adw.PreferencesRow rowDetailsMission;
        [GtkChild]
        public unowned Gtk.Box zoneLancement;
        [GtkChild]
        public unowned Gtk.Box boxImageLanceur;
        [GtkChild]
        public unowned Gtk.Box boxImageAgence;
        [GtkChild]
        public unowned Gtk.Label labelAgence;
        [GtkChild]
        public unowned Gtk.Label labelFusee;
        [GtkChild]
        public unowned Gtk.Label labelMission;
        [GtkChild]
        public unowned Gtk.Label labelStatut;
        [GtkChild]
        public unowned Gtk.Box boxCAR;
        [GtkChild]
        public unowned Gtk.Label labelCAR;
        [GtkChild]
        public unowned Gtk.Label label0000;
        [GtkChild]
        public unowned Gtk.Label labelDate;
        [GtkChild]
        public unowned Gtk.Label labelFenetre;
        [GtkChild]
        public unowned Gtk.Label labelPad;
        [GtkChild]
        public unowned Gtk.Label labelDestination;
        [GtkChild]
        public unowned Gtk.Label labelTypeMission;
        [GtkChild]
        public unowned Gtk.Label labelDetailsMission;
        [GtkChild]
        public unowned Gtk.Box boxBoutonVideo;
        [GtkChild]
        public unowned Gtk.ToggleButton boutonVideo;
        [GtkChild]
        public unowned Gtk.Revealer revealStream;

        public Window win {get; set;}
        public Requete requete {get; set;}

        public Gtk.Picture imageLanceur;
        private Gtk.CssProvider[] css;

        private int decompte;

        public VueLancement (Window win, Requete requete) {
            Object (
                win: win,
                requete: requete
            );
        }

        construct {
            clamp.set_maximum_size(600);
            boxImageLanceur.set_margin_bottom(5);
            zoneLancement.set_spacing(5);
            zoneLancement.set_margin_start(5);
            zoneLancement.set_margin_end(5);
            css = chargementCss();
            rowMission.set_activatable(false);
            rowDetailsMission.set_activatable(false);
        }


		public void disposition(string[] liste, int num) {

		    //NETTOYAGE
            if(win.dev) message("Entrée disposition lancement");
		    effacementPage();

            //RÉCUPÉRATION DE LA BASE DE DONNÉE AGENCE

            string detailAgence = requete.requestData(lectureCle("url",lectureBloc("launch_service_provider",liste[num])));

            //IMAGE LANCEUR

            //IMAGE LANCEUR

            int largeur = int.min(clamp.get_maximum_size(),win.get_width());
            string url = lectureCle("image",liste[num]);
            if(url != null) {
                imageLanceur = creationImageLanceur(url, largeur);
                boxImageLanceur.show();
                boxImageLanceur.append(imageLanceur);
            }


            //IMAGE AGENCE

            Gtk.Picture imageAgence = creationAvatar(lectureCle("nation_url",detailAgence), 84, true);
            boxImageAgence.append(imageAgence);

            //DÉFINITIION DES LABELS

            labelFusee.set_text(lectureCle("name",lectureBloc("rocket",liste[num])));
            string mission = lectureCle("name", liste[num]);
            win.sousTitre.set_text(mission);
            int delimiteur = mission.index_of_char('|');
            labelMission.set_text(mission.substring(delimiteur+2));
            labelAgence.set_text(lectureCle("name",lectureBloc("launch_service_provider",liste[num])));
            string statut = lectureCle("name",lectureBloc("status",liste[num]));
            labelDate.set_text(stringToDateTime(lectureCle("net",liste[num])).format("%d %B %Y"));// - %H:%M");
            string fenetreDebut = stringToDateTime(lectureCle("window_start",liste[num])).format("%H:%M");
            string fenetreFin = stringToDateTime(lectureCle("window_end",liste[num])).format("%H:%M");
            if(fenetreDebut!=fenetreFin) labelFenetre.set_text(fenetreDebut + " - " + fenetreFin);
            if(fenetreDebut==fenetreFin) labelFenetre.set_text(_("Instantaneous launch window") + " - " + fenetreDebut);
            labelDestination.set_text(lectureCle("name",lectureBloc("orbit",lectureBloc("mission",liste[num]))));
            labelTypeMission.set_text(lectureCle("type",lectureBloc("mission",liste[num])));
            labelPad.set_text(lectureCle("name",lectureBloc("location",liste[num])));
            labelDetailsMission.set_text(lectureCle("description",lectureBloc("mission",liste[num])));


            //MODIFICATION STATUT

            labelStatut.set_text(statut);

            bool affichageCAR = false;
            if(statut == "Launch Successful") labelStatut.get_style_context().add_provider(css[0], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            if(statut == "Go for Launch" || statut == "Launch in Flight"){
                affichageCAR = true;
                labelStatut.get_style_context().add_provider(css[0], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            }
            if(statut == "To Be Confirmed" || statut == "To Be Determined") labelStatut.get_style_context().add_provider(css[1], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            if(statut == "On Hold") labelStatut.get_style_context().add_provider(css[2], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
            if(statut == "Launch Failure") labelStatut.get_style_context().add_provider(css[3], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

            //COMPTE À REBOURS

            decompte = 0;
            if(affichageCAR || statut == "Launch Successful"){
                boxCAR.show();
                if(affichageCAR){
                    labelCAR.show();
                    decompte = calculDecompte(lectureCle("net",liste[num]));
                    labelCAR.set_text(decompteToLabel(decompte));
                }
                else {
                    label0000.show();
                }

                //VIDEO STREAM
                //RÉCUPÉRATION DE LA BASE DE DONNÉE URL

                string urlMission = lectureCle("url",liste[num]);

                if(urlMission!=""){
                    string blocMission = requete.requestData(urlMission);
                    string[] vidURLs = lectureVidURLs(lectureBloc2("vidURLs",blocMission));

                    if(vidURLs.length>0){
                        boxBoutonVideo.show();
                        Adw.PreferencesGroup groupe = new Adw.PreferencesGroup();
                        for(int i=0;i<vidURLs.length;i=i+2){
                            Adw.ActionRow ligne = new Adw.ActionRow();
                            ligne.set_title(vidURLs[i+1]);
                            string source = analyseSource(vidURLs[i]);
                            ligne.set_subtitle(source);
                            Gtk.Button bouton = new Gtk.Button();
                            bouton.set_icon_name("media-playback-start-symbolic");
                            bouton.get_style_context().add_class("suggested-action");
                            bouton.set_valign(Gtk.Align.CENTER);
                            ligne.add_suffix(bouton);
                            groupe.add(ligne);
                            string sourceStream = vidURLs[i];
                            string sourceEmbedStream = vidURLs[i];
                            ligne.set_tooltip_text(vidURLs[i]);
                            if(source == "Youtube") sourceEmbedStream = modifUrlYoutube(vidURLs[i]);
                                bouton.clicked.connect(() => {
                                //if(embed && source=="Youtube"){
                                //    deckVues.set_visible_child(vueStream);
                                //    vueStream.load_uri(sourceEmbedStream);
                                //}
                                //else
                                Gtk.show_uri(null, sourceStream, Gdk.CURRENT_TIME);
                            });
                        }
                        revealStream.set_child(groupe);
                        groupe.set_margin_top(10);groupe.set_margin_bottom(10);
                        groupe.set_margin_start(5);groupe.set_margin_end(5);
                        boutonVideo.clicked.connect(() => {basculRevealStream(boutonVideo, revealStream);});
                    }
                }
            }

        }

        private Gtk.Picture creationAvatar(string url, int taille, bool rond) {

            string nom = dossierCache + url.substring(url.last_index_of_char('/'));

            Gdk.Pixbuf pix = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/erreurFichier.svg",taille,taille,true);

                pix =  new Gdk.Pixbuf.subpixbuf (pix,0,0,taille,taille);

                if(url != "null"){
	        	    downloadImage(url,nom);
	        	    try{Gdk.Pixbuf pix1 = new Gdk.Pixbuf.from_file(nom);

	        	    int largeur = pix1.get_width();
	        	    int hauteur = pix1.get_height();

	        	    if(largeur >= hauteur){
	        		    largeur = -1;
	        		    hauteur = taille;
	        	    }
	        	    else{
	        		    largeur = taille;
	        		    hauteur = -1;
	        	    }
                    try{Gdk.Pixbuf pix0 = new Gdk.Pixbuf.from_file_at_scale(nom,largeur,hauteur,true);

                    largeur = pix0.get_width();
                    hauteur = pix0.get_height();

                    int debutX = 0;
                    int debutY = 0;

                    if(largeur > hauteur){
                        debutX = (int) (largeur -taille)/2;
                    }
                    else if(largeur < hauteur){
                        debutY = (int) (hauteur -taille)/2;
                    }
                    pix =  new Gdk.Pixbuf.subpixbuf (pix0,debutX,debutY,taille,taille);} catch(ThreadError e){}
                    } catch(ThreadError e){}
                }

                Gtk.Picture image = new Gtk.Picture ();
	        	image.set_pixbuf(pix);
	        	image.set_size_request(taille, taille);
	        	Gtk.CssProvider cssR = new Gtk.CssProvider ();
                uint8[] arrondi = "* { border-radius: 9999px; }".data;
                cssR.load_from_data (arrondi);
                image.get_style_context().add_provider(cssR, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                image.set_overflow(HIDDEN);

            return image;
        }

        private Gtk.Picture creationImageLanceur(string url, int largeur) {

            string nom = dossierCache + url.substring(url.last_index_of_char('/'));
            if(!File.new_for_path(nom).query_exists()) downloadImage(url,nom);

            Gdk.Pixbuf pix0 = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/erreurFichier.svg",largeur,-1,true);

            try{pix0 = new Gdk.Pixbuf.from_file(nom);} catch(ThreadError e){}

            int largeurPix = pix0.get_width();
            int hauteurPix = pix0.get_height();

            Gdk.Pixbuf pix1 = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/erreurFichier.svg");
            Gdk.Pixbuf pixLanceur = new Gdk.Pixbuf.from_resource("/org/emilien/SpaceLaunch/icons/erreurFichier.svg");

            float ratio = (float) largeurPix / hauteurPix;

            if(ratio<=2.4) {
                int hauteur0 = 720 * hauteurPix / largeurPix;
                pix1 = pix0.scale_simple(720,hauteur0,Gdk.InterpType.BILINEAR);
                int debutY = (int) (hauteur0 - 300)/2;
                pixLanceur = new Gdk.Pixbuf.subpixbuf (pix1,0,debutY,720,300);
            }
            else {
                int largeur0 = 300 * largeurPix / hauteurPix;
                pix1 = pix0.scale_simple(largeur0,300,Gdk.InterpType.BILINEAR);
                int debutX = (int) (largeur0 - 720)/2;
                pixLanceur = new Gdk.Pixbuf.subpixbuf (pix1,debutX,0,720,300);
            }

            Gtk.Picture image = new Gtk.Picture.for_pixbuf(pixLanceur);
            int hauteur = (int) largeur * 300 / 720;
            image.set_size_request(largeur, hauteur);

            return image;
        }

        private void basculRevealStream(Gtk.ToggleButton bouton, Gtk.Revealer revealStream) {

            if(bouton.get_active()) revealStream.set_reveal_child(true);
            else revealStream.set_reveal_child(false);
        }

        public void effacementPage() {

            Gtk.Widget child = boxImageLanceur.get_first_child();
		    while (child!=null){
		        boxImageLanceur.remove(child);
		        child = boxImageLanceur.get_first_child();
		    }

            child = boxImageAgence.get_first_child();
		    while (child!=null){
		        boxImageAgence.remove(child);
		        child = boxImageAgence.get_first_child();
		    }

		    boxImageLanceur.hide();
		    boxBoutonVideo.hide();
            boxCAR.hide();
            label0000.hide();
		    labelCAR.hide();
            revealStream.set_reveal_child(false);
            boutonVideo.set_active(false);
        }

        public bool majCAR() {

            decompte--;
            labelCAR.set_text(decompteToLabel(decompte));
            return true;
        }
    }
}
