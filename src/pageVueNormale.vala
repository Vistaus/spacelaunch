namespace Spacelaunch {
    [GtkTemplate (ui = "/org/emilien/SpaceLaunch/ui/pageVueNormale.ui")]
    public class VueNormale : Gtk.Box {

        [GtkChild]
        public unowned Gtk.ScrolledWindow scrollVueNormale;
        [GtkChild]
        public unowned Adw.Clamp clamp;
        [GtkChild]
        public unowned Gtk.Box listeLancement;
        [GtkChild]
        public unowned Gtk.Revealer revealBoutonPlus;
        [GtkChild]
        public unowned Gtk.Button boutonPlus;

        private Gtk.ListBox[] listeBox;
        private int[] decompte;
        private Gtk.Label[] car;

        public Window win {get; set;}

        public VueNormale (Window win) {
            Object (
                win: win
            );

            scrollVueNormale.edge_reached.connect(affichageBoutonPlus);
            boutonPlus.clicked.connect(win.basculVueCompacte);
            //scrollVueNormale.get_vscrollbar().notify.connect(essai);
        }

        construct {
            clamp.set_maximum_size(600);
        }

        public void disposition(string[] liste) {

            Gtk.CssProvider[] css = chargementCss();

            Pango.AttrList attrs = new Pango.AttrList ();
            attrs.insert (Pango.attr_weight_new (LIGHT));
            Pango.AttrList attrs2 = new Pango.AttrList ();
            attrs2.insert (Pango.attr_weight_new (LIGHT));
            attrs2.insert (Pango.attr_scale_new (0.8));
            Pango.AttrList attrs3 = new Pango.AttrList ();
            attrs3.insert (Pango.attr_weight_new (BOLD));
            attrs3.insert (Pango.attr_scale_new (2));
            attrs3.insert (Pango.attr_family_new("Monospace"));

            listeBox = {}; decompte = {}; car = {};

            //CRÉATION DE LA VUE

            for(int i=0; i<int.min(5,liste.length-1);i++){
                decompte += calculDecompte(lectureCle("net",liste[i]));
                car += new Gtk.Label (decompteToLabel(decompte[i]));

                //BLOCS PRINCIPAUX

                Adw.PreferencesGroup groupe = new Adw.PreferencesGroup();
                Adw.PreferencesRow bloc = new Adw.PreferencesRow();
                Gtk.ListBox ligne0 = new Gtk.ListBox();
                Gtk.ListBoxRow ligne = new Gtk.ListBoxRow();
                ligne0.get_style_context().add_provider(css[5], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                ligne.get_style_context().add_provider(css[5], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                listeBox += ligne0;

                //DÉFINITION DES LABELS

                Gtk.Label labelProvider = new Gtk.Label (lectureCle("name",lectureBloc("launch_service_provider",liste[i])) + " | " + lectureCle("name",lectureBloc("rocket",liste[i])));
                Gtk.Label labelLocation = new Gtk.Label (lectureCle("name",lectureBloc("location",liste[i])));
                Gtk.Label labelDate = new Gtk.Label (stringToDateTime(lectureCle("net",liste[i])).format("%d %B %Y - %H:%M"));
                Gtk.Label labelStatus = new Gtk.Label (lectureCle("name",lectureBloc("status",liste[i])));
                Gtk.Label labelOrbite = new Gtk.Label (lectureCle("abbrev",lectureBloc("orbit",liste[i])));
                string mission = lectureCle("name", liste[i]);
                int delimiteur = mission.index_of_char('|');
                mission = mission.substring(delimiteur+2);
                Gtk.Label labelMission = new Gtk.Label (mission);
                string detailsMission = lectureCle("description",lectureBloc("mission",liste[i]));
                delimiteur = detailsMission.index_of_char('\n');
                if(delimiteur>0 && delimiteur < detailsMission.length) detailsMission = detailsMission.substring(0,delimiteur);
                Gtk.Label labelDetailsMission = new Gtk.Label (detailsMission);
                //Gtk.Button boutonVideo = new Gtk.Button();

                //IMAGE LANCEUR

                string url = lectureCle("image",liste[i]);
                Gtk.Picture image = creationAvatar(url, 100, true);

                //ICONE MISSION

                Gtk.Picture iconeMission = new Gtk.Picture();
                int tailleIcone = 50;

                string typeMission = lectureCle("type",lectureBloc("mission",liste[i]));
                Gdk.Pixbuf pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_unknown.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Astrophysics") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_astrophysics.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Dedicated Rideshare") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_dedicated_rideshare.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Communications") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_communications.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Earth Science") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_earth_science.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Government/Top Secret") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_government_top_secret.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Heliophysics") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_heliophysics.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Human Exploration") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_human_exploration.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Navigation") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_navigation.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Robotic Exploration") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_robotic_exploration.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Resupply") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_resupply.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Test Flight") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_test_flight.svg",tailleIcone,tailleIcone,true);
                if(typeMission=="Tourism") pixMission = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/mission_tourism.svg",tailleIcone,tailleIcone,true);

                iconeMission.set_pixbuf(pixMission);
                iconeMission.set_size_request(tailleIcone,tailleIcone);

                //MISE EN FORME DES LABELS

                labelProvider.set_halign(Gtk.Align.START);
                labelProvider.set_ellipsize(Pango.EllipsizeMode.END);

                labelLocation.set_attributes(attrs);
                labelLocation.set_halign(Gtk.Align.START);
                labelLocation.set_ellipsize(Pango.EllipsizeMode.END);

                labelDate.set_attributes(attrs);
                labelDate.set_halign(Gtk.Align.START);
                labelDate.set_ellipsize(Pango.EllipsizeMode.END);

                labelMission.set_halign(Gtk.Align.START);
                labelMission.set_ellipsize(Pango.EllipsizeMode.END);

                labelDetailsMission.set_attributes(attrs2);
                labelDetailsMission.set_ellipsize(Pango.EllipsizeMode.END);
                labelDetailsMission.set_single_line_mode(true);
                labelDetailsMission.set_wrap(false);
                labelDetailsMission.set_wrap_mode(Pango.WrapMode.WORD_CHAR);
                labelDetailsMission.set_lines(2);
                labelDetailsMission.set_halign(Gtk.Align.START);
                labelDetailsMission.set_justify(Gtk.Justification.FILL);
                labelDetailsMission.set_margin_top(15);
                labelDetailsMission.set_margin_start(5);
                labelDetailsMission.set_margin_end(5);
                labelDetailsMission.set_margin_bottom(10);

                labelOrbite.get_style_context().add_provider(css[8], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                labelOrbite.set_attributes(attrs2);

                //boutonVideo.set_icon_name("video-display-symbolic");

                //MISE EN FORME DU STATUT

                bool status = false;
                if(labelStatus.get_text() == "Launch Successful") labelStatus.get_style_context().add_provider(css[0], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                if(labelStatus.get_text() == "Go for Launch" || labelStatus.get_text() == "Launch in Flight"){
                    status = true;
                    labelStatus.get_style_context().add_provider(css[0], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                }
                if(labelStatus.get_text() == "To Be Confirmed" || labelStatus.get_text() == "To Be Determined") labelStatus.get_style_context().add_provider(css[1], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                if(labelStatus.get_text() == "On Hold") labelStatus.get_style_context().add_provider(css[2], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                if(labelStatus.get_text() == "Launch Failure") labelStatus.get_style_context().add_provider(css[3], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                labelStatus.set_size_request(-1,30);

                //MISE EN FORME DU COMPTE À REBOURS

                if(status){
                    car[i].set_margin_top(10);
                    car[i].set_attributes(attrs3);
                    car[i].get_style_context().add_provider(css[4], Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                }

                //DÉFINITIION DES BOX

                Gtk.Box boite = new Gtk.Box (Gtk.Orientation.VERTICAL,3);
                Gtk.Box boite0 = new Gtk.Box (Gtk.Orientation.HORIZONTAL,2);
                Gtk.Box boite1 = new Gtk.Box (Gtk.Orientation.VERTICAL,3);
                Gtk.Box boite2 = new Gtk.Box (Gtk.Orientation.VERTICAL,3);

                //MISE EN PLACE DES BOX

                boite.append(boite0);
                boite.append(labelStatus);
                if(status) boite.append(car[i]);
                boite.append(labelDetailsMission);

                boite0.append(image);
                boite0.append(boite1);
                boite0.append(boite2);

                boite1.append(iconeMission);
                if(labelOrbite.get_text()!="") boite1.append(labelOrbite);

                boite2.append(labelProvider);
                boite2.append(labelMission);
                boite2.append(labelLocation);
                boite2.append(labelDate);

                boite.set_spacing(5);
                boite0.set_spacing(5);
                boite2.set_spacing(0);
                boite2.set_margin_top(5);boite2.set_margin_bottom(5);
                boite2.set_margin_start(5);boite2.set_margin_end(5);

                //MISE EN PLACE DES BLOCS

                groupe.add(bloc);
                bloc.set_child(ligne0);
                ligne0.append(ligne);
                ligne.set_child(boite);

                if(i==0) groupe.set_margin_top(10);
                groupe.set_margin_bottom(10);
                groupe.set_margin_start(5);
                groupe.set_margin_end(5);
                bloc.set_halign(Gtk.Align.FILL);
                ligne0.get_row_at_index(0).set_selectable(false);
                ligne0.set_name(lectureCle("id",liste[i]));

                listeLancement.append(groupe);
            }
            for(int i=0; i<listeBox.length; i++){
                listeBox[i].row_activated.connect(win.appelLancement);
            }
        }

        public bool majCAR() {

            if(car.length!=0){
                for(int i=0;i<car.length;i++){
                    decompte[i]--;
                    car[i].set_text(decompteToLabel(decompte[i]));
                }
            }
            return true;
		}

		private Gtk.Picture creationAvatar(string url, int taille, bool rond) {

            string nom = dossierCache + url.substring(url.last_index_of_char('/'));

            Gdk.Pixbuf pix = new Gdk.Pixbuf.from_resource_at_scale("/org/emilien/SpaceLaunch/icons/erreurFichier.svg",taille,taille,true);

                pix =  new Gdk.Pixbuf.subpixbuf (pix,0,0,taille,taille);

                if(url != "null"){
	        	    downloadImage(url,nom);
	        	    try{Gdk.Pixbuf pix1 = new Gdk.Pixbuf.from_file(nom);

	        	    int largeur = pix1.get_width();
	        	    int hauteur = pix1.get_height();

	        	    if(largeur >= hauteur){
	        		    largeur = -1;
	        		    hauteur = taille;
	        	    }
	        	    else{
	        		    largeur = taille;
	        		    hauteur = -1;
	        	    }
                    try{Gdk.Pixbuf pix0 = new Gdk.Pixbuf.from_file_at_scale(nom,largeur,hauteur,true);

                    largeur = pix0.get_width();
                    hauteur = pix0.get_height();

                    int debutX = 0;
                    int debutY = 0;

                    if(largeur > hauteur){
                        debutX = (int) (largeur -taille)/2;
                    }
                    else if(largeur < hauteur){
                        debutY = (int) (hauteur -taille)/2;
                    }
                    pix =  new Gdk.Pixbuf.subpixbuf (pix0,debutX,debutY,taille,taille);} catch(ThreadError e){}
                    } catch(ThreadError e){}
                }

                Gtk.Picture image = new Gtk.Picture ();
	        	image.set_pixbuf(pix);
	        	image.set_size_request(taille, taille);
	        	Gtk.CssProvider cssR = new Gtk.CssProvider ();
                uint8[] arrondi = "* { border-radius: 9999px; }".data;
                cssR.load_from_data (arrondi);
                image.get_style_context().add_provider(cssR, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
                image.set_overflow(HIDDEN);

            return image;
        }

		private void affichageBoutonPlus(Gtk.PositionType position) {

            if(position == Gtk.PositionType.BOTTOM){
		        revealBoutonPlus.set_reveal_child(true);
		    }
		    else if(position == Gtk.PositionType.TOP){
		        revealBoutonPlus.set_reveal_child(false);
		    }
		}

        public void effacementPage() {

            Gtk.Widget child = listeLancement.get_first_child();
		        while (child!=null){
		            listeLancement.remove(child);
		            child = listeLancement.get_first_child();
		        }
        }
    }
}
