namespace Spacelaunch{

    void downloadImage(string url, string nom) {

        if(!GLib.FileUtils.test(nom, GLib.FileTest.EXISTS)){
            var session = new Soup.Session ();
	        Soup.Request request = session.request (url);
	        InputStream stream = request.send ();
	        File file = File.new_for_path (nom);
            FileOutputStream os = file.create (FileCreateFlags.REPLACE_DESTINATION);
            os.splice (stream, OutputStreamSpliceFlags.CLOSE_TARGET);
        }
    }
}
